package com.example.pregunta_dia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {
    private Button btn_responder; // instancia de boton responder
    private RadioGroup rg_grupo; // instancia del radiogroup

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //asocio las instancias a los elementos del xml
        btn_responder = findViewById(R.id.respuesta);
        rg_grupo = findViewById(R.id.grupo1);

        //asigno un listener al boton enviar
        btn_responder.setOnClickListener(enviarListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    //creacion de enviarListener
    private View.OnClickListener enviarListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //recupero el valor del radiobutton seleccionado
            int radioButtonId = rg_grupo.getCheckedRadioButtonId();
            View radioButton = rg_grupo.findViewById(radioButtonId);

            //indica el indice de que radioButton fue seleccionado para ese radiogroup
            int idx = rg_grupo.indexOfChild(radioButton);

            //abro la otra actividad con intent
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            //creacion de bundle para pasar datos entre activity

            intent.putExtra("opcion", idx);
            startActivity(intent);
        }
    };
}