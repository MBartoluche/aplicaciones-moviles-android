package com.example.pregunta_dia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private TextView txt_mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        int opcion;
        //texto_mensaje id del textview del activity_second.xml
        txt_mensaje = findViewById(R.id.texto_mensaje);

        //recupero el dato del bundle del MainActivity
        Bundle bundle = getIntent().getExtras();
        // opcion guarda un int con el id de la opcion seleccionada en MainActivity
        opcion = bundle.getInt("opcion");

        actualizarMensaje(opcion);
    }
    private void actualizarMensaje(int opcion){
        switch (opcion){
            case 0:
                txt_mensaje.setText(R.string.respuesta_excelente);
                break;
            case 1:
                txt_mensaje.setText(R.string.respuesta_muy_bien);
                break;
            case 2:
                txt_mensaje.setText(R.string.respuesta_bien);
                break;
            case 3:
                txt_mensaje.setText(R.string.respuesta_regular);
                break;
            case 4:
                txt_mensaje.setText(R.string.respuesta_mal);
                break;
        }
    }
}