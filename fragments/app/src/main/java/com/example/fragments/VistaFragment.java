package com.example.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

public class VistaFragment extends Fragment {
    WebView webview;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        // recupero el bundleURL del fragment manager
        getParentFragmentManager().setFragmentResultListener("obtenerUrl", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle bundleResult) {
                // recupero el link del bundleURL del UrlFragment
                String urlRecuperado = bundleResult.getString("urlObtenida"); //

                webview.setWebViewClient(new WebViewClient());// instancia de webViewClient
                webview.loadUrl(urlRecuperado); // ingreso el url al webview
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState){
       return inflater.inflate(R.layout.fragment_vista, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        webview = view.findViewById(R.id.web);
    }
}
