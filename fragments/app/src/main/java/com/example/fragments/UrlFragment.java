package com.example.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class UrlFragment extends Fragment {
    Button btn;
    EditText edit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState){
        View view = inflater.inflate(R.layout.fragment_url, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        edit = view.findViewById(R.id.capturar);
        btn = view.findViewById(R.id.enviar);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundleURL = new Bundle();// creacion de bundle para pasar datos al siguiente fragment

                // cargo el bundle con el string capturado del edittext
                bundleURL.putString("urlObtenida", edit.getText().toString().trim());

                //guardo el bundleURL con el fragment manager
                getParentFragmentManager().setFragmentResult("obtenerUrl", bundleURL);
            }
        });
    }
}
