package com.example.maestrodetalle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdaptadorItem extends RecyclerView.Adapter<AdaptadorItem.ItemViewHolder> implements View.OnClickListener {

    ArrayList<ItemLista> listaItem; // lista que contendra instancias de objetos "ItemLista"
    private View.OnClickListener listener;

    public AdaptadorItem(ArrayList<ItemLista> lista){
        this.listaItem = lista; // guardo la lista en la variable listaItem
    }

    @NonNull
    @Override
    public AdaptadorItem.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista, null, false);
        view.setOnClickListener(this);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorItem.ItemViewHolder holder, int position) {
        //paso como parametro un ItemViewHolder
        // obtengo los datos de posicion y nombre de cada objeto item desde el holder
        holder.textoItemPosicion.setText(listaItem.get(position).getPosicion());
        holder.textoItemNombre.setText(listaItem.get(position).getNombre());
    }

    @Override
    public int getItemCount() {
        return listaItem.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view){
        //si el listener no es null
        if(listener != null){
            listener.onClick(view);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView textoItemNombre, textoItemPosicion; // textview del item_fragment.xml

        public ItemViewHolder(View itemView){
            super(itemView);
            textoItemPosicion = (TextView) itemView.findViewById(R.id.posicion); //obtengo sus id
            textoItemNombre = (TextView) itemView.findViewById(R.id.nombre);
        }
    }
}
