package com.example.maestrodetalle;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaItemFragment extends Fragment {

    ArrayList<ItemLista> listaDeItems;
    RecyclerView recyclerItems;

    Activity actividad;
    ComunicarFragments comunicar;

    public ListaItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_lista_item, container, false);

        listaDeItems = new ArrayList<>();
        // obtengo el id del recyclerview
        recyclerItems = vista.findViewById(R.id.recyclerListaItem);
        // cargo el recyclerview con un nuevo LinearLayoutManager
        recyclerItems.setLayoutManager(new LinearLayoutManager(getContext()));
        
        llenarListaItems(); //llamo al metodo llenarListaItems

        //instancia de la clase AdaptadorItem, paso como parametro la lista de item
        AdaptadorItem adaptador = new AdaptadorItem(listaDeItems);

        //setAdapter recibe un adaptador como parametro, adaptador extiende deRecyclerView.adapter
        recyclerItems.setAdapter(adaptador);

        //cada vez que se hace click en un item de la lista
        adaptador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Abriendo " + listaDeItems.get(recyclerItems.getChildAdapterPosition(view)).getNombre(), Toast.LENGTH_SHORT).show();
                //enviar el objeto completo al otro fragment
                // llamo a la interface
                comunicar.enviarItem(listaDeItems.get(recyclerItems.getChildAdapterPosition(view)));
            }
        });
        return vista;
    }

    //agrego algunos objetos ItemLista a la lista
    private void llenarListaItems() {
        listaDeItems.add(new ItemLista("1", "Google", "www.google.com"));
        listaDeItems.add(new ItemLista("2", "Amazon", "www.amazon.com"));
        listaDeItems.add(new ItemLista("3", "Android", "developer.android.com/docs"));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //si el contexto es una instancia de la actividad en la que esta cargado este fragment
        if(context instanceof Activity){
            this.actividad = (Activity) context;
            comunicar = (ComunicarFragments) this.actividad;
        }
    }
}