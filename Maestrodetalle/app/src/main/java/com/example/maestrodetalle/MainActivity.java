package com.example.maestrodetalle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements ComunicarFragments{
    ListaItemFragment listaFragment;
    DetalleItemFragment detalleFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaFragment = new ListaItemFragment(); //instancia de ListaItemFragment

        //cargar el fragment al activity
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaccion = fragmentManager.beginTransaction();

        //contenedorFragment es el id del frame layout del main_activity.xml
        //replace tiene como parametro el id del FrameLayout y el id del fragment que quiero cargar
        transaccion.replace(R.id.contenedorFragment, listaFragment);
        transaccion.commit();
    }

    @Override
    public void enviarItem(ItemLista item) {
        detalleFragment = new DetalleItemFragment(); //crear instancia de DetalleItemFragment
        Bundle enviar = new Bundle(); // crear un bundle para enviar el objeto Item al siguiente fragment

        enviar.putSerializable("objeto", item); // cargo el objeto con el item que paso como parametro
        detalleFragment.setArguments(enviar); // enviar el objeto

        //cargar el fragment al activity
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contenedorFragment, detalleFragment)
                .addToBackStack(null).commit();
    }
}