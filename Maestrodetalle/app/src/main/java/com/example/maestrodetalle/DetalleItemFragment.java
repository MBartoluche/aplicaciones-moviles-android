package com.example.maestrodetalle;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class DetalleItemFragment extends Fragment {

    TextView posicion, nombre, descripcion;
    WebView pagina;

    public DetalleItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_detalle_item, container, false);

        //obtengo los id de los elementos del fragment_detalle_item.xml
        posicion = vista.findViewById(R.id.posicionFragmentDetalle);
        nombre = vista.findViewById(R.id.nombreFragmentDetalle);
        descripcion = vista.findViewById(R.id.detalleFragmentDetalle);
        pagina = vista.findViewById(R.id.webview);

        //recibir el objeto enviado
        Bundle recibir = getArguments();

        ItemLista item;

        if(recibir != null){
            //si el bundle recibir no es nulo, recupero el objeto
            item = (ItemLista) recibir.getSerializable("objeto");

            //cargo los datos del objeto a los elementos del fragment_detalle_item.xml
            posicion.setText(item.getPosicion());
            nombre.setText(item.getNombre());
            descripcion.setText(item.getDescripcion());

            pagina.setWebViewClient(new WebViewClient());
            pagina.loadUrl(item.getDescripcion());
        }
        return vista;
    }
}