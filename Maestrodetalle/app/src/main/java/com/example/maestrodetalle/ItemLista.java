package com.example.maestrodetalle;

import java.io.Serializable;

//clase ItemLista
//Serializable para poder pasar el objeto de un fragment a otro
public class ItemLista implements Serializable {
    private String posicion, nombre, descripcion;
    
    public ItemLista(){
    }

    public ItemLista(String posicion, String nombre, String descripcion){
        this.posicion = posicion;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getPosicion(){
        return posicion;
    }

    public void setPosicion(String nuevaPosicion){
        posicion = nuevaPosicion;
    }

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nuevoNombre){
        posicion = nuevoNombre;
    }

    public String getDescripcion(){ return descripcion; }

    public void setDescripcion(String nuevaDescripcion){ descripcion = nuevaDescripcion; }
}
