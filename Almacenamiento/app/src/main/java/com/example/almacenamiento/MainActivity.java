package com.example.almacenamiento;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    private final String prefName = "Font";
    private static final String FONT_SIZE_KEY = "size";
    private static final String TEXT_VALUE_KEY = "value";

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*------------------------------- Inicio Shared Preferences ---------------------------------*/
        Button btn;
        EditText edit;
        SeekBar seek;

        btn = findViewById(R.id.botonShared);
        edit = findViewById(R.id.editTextShared);
        seek = findViewById(R.id.seekBar);

        // cargo el objeto con modo privado
        final SharedPreferences prefer = getSharedPreferences(prefName, MODE_PRIVATE);

        //guardo el tamanio del texto
        float fontSize = prefer.getFloat(FONT_SIZE_KEY, 12);

        //guardo el progreso del seekBar
        seek.setProgress((int) fontSize);

        //cambio el valor del editText con setText, prefer.getString obtiene el valor de TEXT_VALUE_KEY
        edit.setText(prefer.getString(TEXT_VALUE_KEY, ""));

        //cambio el tamaño del editText con setTextSize, getProgress obtiene el progreso del seekBar
        edit.setTextSize(seek.getProgress());

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                edit.setTextSize(i); // cambio el tamaño del editText segun el parametro i
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //obtengo el objeto SharedPreferences con el get
                SharedPreferences prefs = getSharedPreferences(prefName, MODE_PRIVATE);
                //creacion del objeto shared Preferences Sditor
                SharedPreferences.Editor e = prefs.edit();

                //guardo el texto del EditText edit
                e.putFloat(FONT_SIZE_KEY, edit.getTextSize());
                e.putString(TEXT_VALUE_KEY, edit.getText().toString());
                e.apply(); // guardo

                Toast.makeText(getBaseContext(), "Datos guardados", Toast.LENGTH_SHORT).show();
            }
        });
        /*------------------------------- Fin Shared Preferences ---------------------------------*/

        /*-------------------------------Inicio file storage -------------------------------------*/

        EditText editTextFile;
        Button btn_guardar, btn_recuperar;

        editTextFile = findViewById(R.id.editTextFile);
        btn_guardar = findViewById(R.id.botonGuardarFile);
        btn_recuperar = findViewById(R.id.botonRecuperarFile);

        btn_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String texto = editTextFile.getText().toString();

                try{
                    //creacion del archivo en modo privado
                    FileOutputStream archivo = openFileOutput("archivo.txt", MODE_PRIVATE);
                    //creacion del objeto OutputStreamWriter
                    OutputStreamWriter escribir = new OutputStreamWriter(archivo);

                    escribir.write(texto);
                    escribir.flush();
                    escribir.close();

                    Toast.makeText(getBaseContext(), "Archivo guardado", Toast.LENGTH_SHORT).show();
                    editTextFile.setText("");
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btn_recuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    FileInputStream abrirArchivo = openFileInput("archivo.txt");
                    InputStreamReader leer = new InputStreamReader(abrirArchivo);
                    BufferedReader buf = new BufferedReader(leer);

                    String linea = buf.readLine();
                    String s = "";
                    while(linea != null){
                        s += linea + "\n";// concatenar el texto leido a la variable
                        linea = buf.readLine();
                    }
                    buf.close();//cerrar buffer
                    leer.close();//cerrar el lector de archivo

                    //cargar el valor al editText
                    editTextFile.setText(s);
                }
                catch(IOException e){
                    e.printStackTrace();
                }

            }
        });
        /*---------------------------------------- Fin file storage ------------------------------*/

        /*------------------------------------ inicio SQLite -------------------------------------*/
        EditText edit_buscar_SQLite = findViewById(R.id.editBuscarSQLite);
        Button btb_buscar_sqlite = findViewById(R.id.botonBuscarSQLite);
        Button btn_eliminar_sqlite = findViewById(R.id.botonEliminarSQLite);
        EditText edit_nombre = findViewById(R.id.editNombreSQLite);
        EditText edit_correo = findViewById(R.id.editCorreoSQLite);
        Button btn_alta = findViewById(R.id.botonAltaSQLite);
        Button btn_modif = findViewById(R.id.botonModificarSQLite);

        DataBaseHelper helper = new DataBaseHelper(this);

        //click boton alta
        btn_alta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = edit_nombre.getText().toString(); //tomamoe el texto de los campor nombre y correo
                String correo = edit_correo.getText().toString();

                ContentValues registro = new ContentValues(); // creacion del contentValues
                registro.put("nombre" ,nombre); // cargar el ContentValues con los textos capturados de los edittext
                registro.put("correo", correo); // con clave y valor

                SQLiteDatabase bd = helper.open(); // abro la base de datos
                bd.insert("contactos", null, registro); // guardo en la base de datos "contactos" el ContentValues
                bd.close();// cierro la base de datos

                Toast.makeText(getBaseContext(), "Contacto guardado", Toast.LENGTH_SHORT).show();
                edit_nombre.setText("");
                edit_correo.setText("");
            }
        });

        //eliminar contacto
        btn_eliminar_sqlite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String buscar = edit_buscar_SQLite.getText().toString(); // capturo el texto del edit text buscar

                SQLiteDatabase bd = helper.open(); // abro la base de datos

                int cant = bd.delete("contactos", "nombre='"+buscar+"'", null); //
                if(cant > 0){
                    Toast.makeText(getBaseContext(), "Contacto borrado", Toast.LENGTH_SHORT).show(); // si se cumple la condicion, elimina
                }
                else{
                    Toast.makeText(getBaseContext(), "No existe el contacto", Toast.LENGTH_SHORT).show();
                }
                edit_buscar_SQLite.setText("");
                edit_nombre.setText("");
                edit_correo.setText("");
                bd.close();
            }
        });

        //modificar contacto
        btn_modif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = edit_nombre.getText().toString(); // capturo los textos de ls edit text nombre y correo
                String correo = edit_correo.getText().toString();

                SQLiteDatabase bd = helper.open(); // abro la base de datos

                ContentValues registro = new ContentValues(); // creacion del ContentValues
                registro.put("correo", correo); // cargo el ContentValues con el correo

                int cant = bd.update("contactos", registro, "nombre='"+nombre+"'", null);

                if(cant > 0){
                    Toast.makeText(getBaseContext(), "Contacto modificado", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "No existe el contacto", Toast.LENGTH_SHORT).show();
                }
                bd.close();
                edit_nombre.setText("");
                edit_correo.setText("");
            }
        });

        btb_buscar_sqlite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String buscar = edit_buscar_SQLite.getText().toString(); // capturo el texto del edit text buscar

                //armo la consulta SQL con el texto capturado del edit text
                String consulta = "SELECT nombre, correo FROM contactos WHERE (nombre like '"+buscar+"%')";

                SQLiteDatabase bd = helper.open(); // abro la base de datos
                Cursor fila = bd.rawQuery(consulta, null); // fila va a revisar cada fila de la base de datos

                if(fila.moveToFirst()){
                    edit_nombre.setText(fila.getString(0)); // obtengo el string de nombre
                    edit_correo.setText(fila.getString(1)); // obtengo el string de correo
                }
                else{
                    edit_nombre.setText("");
                    edit_correo.setText("");
                    Toast.makeText(getBaseContext(), "No existe el contacto",Toast.LENGTH_SHORT).show();
                }
                fila.close();// libero el cursor
                bd.close(); // cierro la base de datos
                edit_buscar_SQLite.setText("");
            }
        });
    }
}