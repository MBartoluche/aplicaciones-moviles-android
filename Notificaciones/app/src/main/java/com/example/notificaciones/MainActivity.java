package com.example.notificaciones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.DialogFragment;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements MultipleChoiceDialogFragment.onMultiChoiceListener{
    //creacion de Strings que se usara para crear un canal, para android 8 o superior
    private final String channelId = "miCanal";
    private final String channelName = "NombreCanal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //---------------------------------- inicio notificacion toast -----------------------------
        Button btn_toast = findViewById(R.id.boton_toast);

        btn_toast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Notificacion toast", Toast.LENGTH_SHORT).show();
            }
        });
        //--------------------------------- fin notificacion toast ---------------------------------

        //--------------------------------- inicio notificacion status bar -------------------------
        createNotificationChannel();// llamo a funcion

        //creacion de la notidicacion
        //this(context), channelId es el canal de comunicacion para la notificacion
        NotificationCompat.Builder notificacion =
                new  NotificationCompat.Builder(this, channelId)
                        .setContentTitle("Titulo de notificacion")
                        .setContentText("Contenido de notificacion")
                        .setSmallIcon(R.drawable.ic_notificacion_test)
                        .setPriority(NotificationCompat.PRIORITY_MAX);

        Button btn_notificacion = findViewById(R.id.boton_notificacion);

        btn_notificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationManager notifManag = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notifManag.notify(0, notificacion.build());
            }
        });
        //----------------------------------- fin notificacion status bar --------------------------

        //----------------------------------- inicio alert dialog ----------------------------------

        Button btn_alerta = findViewById(R.id.boton_alerta);

        btn_alerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(MainActivity.this);
                alerta.setTitle("Titulo de alerta");
                alerta.setMessage("Mensaje de alerta");
                alerta.setPositiveButton("OK",null);
                alerta.show();
            }
        });

        //----------------------------------- fin alert dialog -------------------------------------

        //----------------------------------- inicio Alert dialog opciones  ------------------------

        Button btn_alerta_opcion = findViewById(R.id.boton_alerta_opcion);

        btn_alerta_opcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence [] opciones = {"Opcion 1", "Opcion 2", "Opcion 3"};

                AlertDialog.Builder alerta = new AlertDialog.Builder(MainActivity.this);
                alerta.setTitle("Titulo de alerta opciones");
                alerta.setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int indice) {
                        Toast.makeText(getBaseContext(), "Seleccionaste "+opciones[indice].toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                alerta.show();
            }
        });
        //--------------------------------- fin Alert dialog opciones ------------------------------

        //--------------------------------- iicio Alert checkbox -----------------------------------
        Button btn_alerta_check = findViewById(R.id.boton_alerta_opciones_check);

        btn_alerta_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // instancia de MultipleChoiceDialogFragment
                DialogFragment alerta = new MultipleChoiceDialogFragment();

                alerta.setCancelable(false);
                alerta.show(getSupportFragmentManager(), "Multichoice dialog");
            }
        });

        //--------------------------------- fin Alert checkbox -------------------------------------
    }

    // metodo para crear canal de comunicaciones para el status bar notification
    private void createNotificationChannel() {
        //a partir de android 8, las notificaciones se transmiten por canales
        //este if compara la version del android del celular en que se instalo
        //y crea un canal de ser necesario
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            manager.createNotificationChannel(channel);
        }
    }

    // metodos para el Alert con checkbox
    @Override
    public void onPositiveButtonClicked(String[] lista, ArrayList<String> seleccionados) {
        // instancia de stringBuilder para armar un string con las opciones seleccionadas
        StringBuilder stringBuilder = new StringBuilder();

        for(String str:seleccionados){
            stringBuilder.append(str); // agrego las opciones seleccionadas al stringBuilder
            stringBuilder.append(" ");
        }
        Toast.makeText(getBaseContext(),"Seleccionaste: "+stringBuilder,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNegativeButtonClicked() {
        Toast.makeText(getBaseContext(),"Cancelado",Toast.LENGTH_SHORT).show();
    }
}