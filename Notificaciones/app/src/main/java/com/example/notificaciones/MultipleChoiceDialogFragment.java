package com.example.notificaciones;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

public class MultipleChoiceDialogFragment extends DialogFragment {

    //crear interface para utilizar en el MainActivity
    public interface onMultiChoiceListener{
        void onPositiveButtonClicked(String[] lista, ArrayList<String> seleccionados);
        void onNegativeButtonClicked();
    }

    onMultiChoiceListener miListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            miListener = (onMultiChoiceListener) context;
        }
        catch (Exception e){
            throw new ClassCastException(getActivity().toString()+" onMultiChoiceListener debe ser implementado");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // crear un arraylist que almacenara los checkbox seleccionados
        ArrayList<String> seleccionados = new ArrayList<>();

        // crear un AlertDialog.Builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //cargo el <string-array> del archivo strings.xml a la lista
        String[] lista = getActivity().getResources().getStringArray(R.array.elegir_items);

        builder.setTitle("Titulo opcion checkbox");
        // paso la lista de opciones como parametro al setMultiChoiceItems
        // null para que se inicie sin ningun item seleccionado
        // new para crear el ClickListener
        builder.setMultiChoiceItems(lista, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                if(b){
                    seleccionados.add(lista[i]); // agrego el item a la lista de seleccionados
                }
                else{
                    seleccionados.remove(lista[i]); // retiro el item
                }
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                miListener.onPositiveButtonClicked(lista, seleccionados);
            }
        });

        builder.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                miListener.onNegativeButtonClicked();
            }
        });
        return builder.create();
    }
}
