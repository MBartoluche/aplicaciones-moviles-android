package com.example.practicabuenosmodales;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {
    private Button btn_gracias, btn_estornudar; // crear instancias de botones
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //obtengo los id de los botones
        btn_gracias = (Button)findViewById(R.id.boton_gracias);
        btn_estornudar = (Button)findViewById(R.id.boton_estornudar);

        //asigno clickListener para cada boton
        btn_gracias.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View view){
                //mensaje toast
                Toast.makeText(MainActivity.this, "De nada", Toast.LENGTH_LONG).show();
            }
        });

        btn_estornudar.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(MainActivity.this, "Salud", Toast.LENGTH_LONG).show();
            }
        });
    }
}